package cop5556fa19.AST;

import cop5556fa19.Token;

public class Name extends ASTNode {

  public final String name;

  public Name(Token firstToken, String name) {
    super(firstToken);
    this.name = name;
  }

  public Name(Token firstToken) {
    super(firstToken);
    this.name = firstToken.text;
  }

  @Override
  public String toString() {
    return "Name [name=" + name + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Name other = (Name)obj;
    if (name == null) {
      return other.name == null;
    } else
      return name.equals(other.name);
  }

  @Override
  public Object visit(ASTVisitor v, Object arg) throws Exception {
    return v.visitName(this, arg);
  }
}
