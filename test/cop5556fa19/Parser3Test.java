package cop5556fa19;

// import com.google.gson.Gson;
// import com.google.gson.GsonBuilder;

import cop5556fa19.AST.*;
import org.junit.jupiter.api.Test;

import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Parser3Test {

  // To make it easy to print objects and turn this output on and off
  static final boolean doPrint = true;
  //	static final boolean doPrint = false;

  private void show(Object input) {
    if (doPrint) {
      System.out.println(input.toString());
    }
  }

  // creates a scanner, parser, and parses the input by calling exp().
  Exp parseExpAndShow(String input) throws Exception {
    show("parser input:\n" + input); // Display the input
    Reader r = new StringReader(input);
    Scanner scanner = new Scanner(r); // Create a Scanner and initialize it
    Parser parser = new Parser(scanner);
    Exp e = parser.exp();
    show("e=" + e);
    return e;
  }

  // creates a scanner, parser, and parses the input by calling block()
  Block parseBlockAndShow(String input) throws Exception {
    show("parser input:\n" + input); // Display the input
    Reader r = new StringReader(input);
    Scanner scanner = new Scanner(r); // Create a Scanner and initialize it
    Parser parser = new Parser(scanner);
    Method method = Parser.class.getDeclaredMethod("block");
    method.setAccessible(true);
    Block b = (Block)method.invoke(parser);
    show("b=" + b);
    return b;
  }

  // creates a scanner, parser, and parses the input by calling parse()
  // this corresponds to the actual use case of the parser
  Chunk parseAndShow(String input) throws Exception {
    show("parser input:\n" + input); // Display the input
    Reader r = new StringReader(input);
    Scanner scanner = new Scanner(r); // Create a Scanner and initialize it
    Parser parser = new Parser(scanner);
    Chunk c = parser.parse();

    // Gson gson = new GsonBuilder().setPrettyPrinting().create();
    // show("c=" + gson.toJson(c));
    show("c=" + c);
    return c;
  }

  @Test
  void testEmpty1() throws Exception {
    String input = ";";
    Block b = parseBlockAndShow(input);
    Block expected = Expressions.makeBlock();
    assertEquals(expected, b);
  }

  @Test
  void testEmpty2() throws Exception {
    String input = "";
    ASTNode n = parseAndShow(input);
    Block b = Expressions.makeBlock();
    Chunk expected = new Chunk(b.firstToken, b);
    assertEquals(expected, n);
  }

  @Test
  void testFreeStyle0() throws Exception {
    String input = "local p = {}\n"
                   + " \n"
                   + "function p.conditions()\n"
                   + "    local hour\n"
                   + "    local result\n"
                   + " \n"
                   + "    hour = tonumber(os.date('%H'))\n"
                   + "    if hour < 12 then\n"
                   + "        result = 'Good morning!'\n"
                   + "    elseif hour < 18 then\n"
                   + "        result = 'Good afternoon!'\n"
                   + "    else\n"
                   + "        result = 'Good evening!'\n"
                   + "    end\n"
                   + " \n"
                   + "    return result\n"
                   + "end\n"
                   + " \n"
                   + "return p1"; // +
    //"print(fourplus(3))"; //functioncall not a statement for now
    Chunk b = parseAndShow(input);
  }

  @Test
  void testFreeStyle1() throws Exception {
    String input = "local function addto(x)\n"
                   + "  return function(y)\n"
                   + "    return x + y\n"
                   + "  end\n"
                   + "end\n"
                   + "\n"
                   + "fourplus = addto(4)\n"; // +
    //"print(fourplus(3))"; //functioncall not a statement for now
    Block b = parseBlockAndShow(input);
  }

  // from https://en.wikiversity.org/wiki/Lua/Conditions
  // and https://en.wikiversity.org/wiki/Lua/Tables
  @Test
  void testFreeStyle2() throws Exception {
    String input =
        "local p = {} \n function p.forloop()\n"
        + "    local i\n"
        + "    local result\n"
        + " \n"
        + "    result = ';for\\n'\n"
        + "    for i = 2, 10, 2 do\n"
        + "        result = result .. \":i = \" .. i .. '\\n'\n"
        + "    end\n"
        + " \n"
        + "    return result\n"
        + "end \n function p.whileloop()\n"
        + "    local i\n"
        + "    local result\n"
        + " \n"
        + "    result = ';while\\n'\n"
        + "    i = 2\n"
        + "    while i <= 10 do \n"
        + "        result = result .. \":i = \" .. i .. '\\n'\n"
        + "        i = i + 2\n"
        + "    end\n"
        + " \n"
        + "    return result\n"
        + "end \n function p.repeatloop()\n"
        + "    local i\n"
        + "    local result\n"
        + " \n"
        + "    result = ';repeat\\n'\n"
        + "    i = 2\n"
        + "    repeat \n"
        + "        result = result .. \":i = \" .. i .. '\\n'\n"
        + "        i = i + 2\n"
        + "    until i > 10\n"
        + " \n"
        + "    return result\n"
        + "end\n"
        + " \n"
        + "local function tableToString(t)\n"
        + "    local key\n"
        + "    local value\n"
        + "    local result\n"
        + " \n"
        + "    result = ''\n"
        + " \n"
        + "    for key, value in pairs(t) do\n"
        + "        if (tonumber(key) ~= nil) then\n"
        +
        "            result = result .. ':table[' .. key .. '] is ' .. value .. '\\n' \n"
        + "        else\n"
        +
        "            result = result .. ':table[\\'' .. key .. '\\'] is ' .. value .. '\\n' \n"
        + "        end\n"
        + "    end\n"
        + " \n"
        + "    return result\n"
        + "end\n"
        + " \n"
        + "function p.sequence()\n"
        + "    local numbers = {10, 20, 30}\n"
        + "    local result\n"
        + " \n"
        + "    result = ';sequence\\n'\n"
        + "    result = result .. tableToString(numbers)\n"
        + " \n"
        + "    return result\n"
        + "end\n"
        + " \n"
        + "function p.dictionary()\n"
        + "    local languages = {\n"
        + "        ['de'] = 'German',\n"
        + "        ['en'] = 'English', \n"
        + "        ['es'] = 'Spanish', \n"
        + "        ['fr'] = 'French',\n"
        + "        ['it'] = 'Italian',\n"
        + "        ['ja'] = 'Japanese',\n"
        + "        ['ko'] = 'Korean',\n"
        + "        ['ru'] = 'Russian',\n"
        + "        ['zh'] = 'Chinese'\n"
        + "    }\n"
        + "    local result\n"
        + " \n"
        + "    result = ';dictionary\\n'\n"
        + "    result = result .. tableToString(languages)\n"
        + " \n"
        + "    return result\n"
        + "end\n"
        + "return p";
    parseAndShow(input);
  }

  @Test
  void testFreeStyle4() throws Exception {
    // String input = "a = f:a{a=b}"; // +
    //"print(fourplus(3))"; //functioncall not a statement for now
    String input = "if x == 123 then a = b elseif x == 321 then a = c end";
    parseAndShow(input);
  }

  @Test
  void testAssign1() throws Exception {
    String input = "a=b";
    Block b = parseBlockAndShow(input);
    List<Exp> lhs = Expressions.makeExpList(Expressions.makeExpName("a"));
    List<Exp> rhs = Expressions.makeExpList(Expressions.makeExpName("b"));
    StatAssign s = Expressions.makeStatAssign(lhs, rhs);
    Block expected = Expressions.makeBlock(s);
    assertEquals(expected, b);
  }

  @Test
  void testAssignChunk1() throws Exception {
    String input = "a = b";
    ASTNode c = parseAndShow(input);
    List<Exp> lhs = Expressions.makeExpList(Expressions.makeExpName("a"));
    List<Exp> rhs = Expressions.makeExpList(Expressions.makeExpName("b"));
    StatAssign s = Expressions.makeStatAssign(lhs, rhs);
    Block b = Expressions.makeBlock(s);
    Chunk expected = new Chunk(b.firstToken, b);
    assertEquals(expected, c);
  }

  @Test
  void testAssignChunk2() throws Exception {
    String input = "a = b().c;";
    ASTNode c = parseAndShow(input);
    //    List <Exp> lhs =
    //    Expressions.makeExpList(Expressions.makeExpName("a")); List <Exp> rhs
    //    = Expressions.makeExpList(Expressions.makeExpName("b")); StatAssign s
    //    = Expressions.makeStatAssign(lhs, rhs); Block b =
    //    Expressions.makeBlock(s); Chunk expected = new Chunk(b.firstToken, b);
    //    assertEquals(expected, c);
  }
  @Test
  void testMultiAssign1() throws Exception {
    String input = "a,c=8,9";
    Block b = parseBlockAndShow(input);
    List<Exp> lhs = Expressions.makeExpList(Expressions.makeExpName("a"),
                                            Expressions.makeExpName("c"));
    Exp e1 = Expressions.makeExpInt(8);
    Exp e2 = Expressions.makeExpInt(9);
    List<Exp> rhs = Expressions.makeExpList(e1, e2);
    StatAssign s = Expressions.makeStatAssign(lhs, rhs);
    Block expected = Expressions.makeBlock(s);
    assertEquals(expected, b);
  }

  @Test
  void testMultiAssign3() throws Exception {
    String input = "a,c=8,f(x)";
    Block b = parseBlockAndShow(input);
    List<Exp> lhs = Expressions.makeExpList(Expressions.makeExpName("a"),
                                            Expressions.makeExpName("c"));
    Exp e1 = Expressions.makeExpInt(8);
    List<Exp> args = new ArrayList<>();
    args.add(Expressions.makeExpName("x"));
    Exp e2 =
        Expressions.makeExpFunCall(Expressions.makeExpName("f"), args, null);
    List<Exp> rhs = Expressions.makeExpList(e1, e2);
    StatAssign s = Expressions.makeStatAssign(lhs, rhs);
    Block expected = Expressions.makeBlock(s);
    assertEquals(expected, b);
  }

  @Test
  void testAssignToTable() throws Exception {
    String input = "g.a.b = 3";
    Block bl = parseBlockAndShow(input);
    ExpName g = Expressions.makeExpName("g");
    ExpString a = Expressions.makeExpString("a");
    Exp gtable = Expressions.makeExpTableLookup(g, a);
    ExpString b = Expressions.makeExpString("b");
    Exp v = Expressions.makeExpTableLookup(gtable, b);
    Exp three = Expressions.makeExpInt(3);
    Stat s = Expressions.makeStatAssign(Expressions.makeExpList(v),
                                        Expressions.makeExpList(three));
    Block expected = Expressions.makeBlock(s);
    assertEquals(expected, bl);
  }

  @Test
  void testAssignTableToVar() throws Exception {
    String input = "x = g.a.b";
    Block bl = parseBlockAndShow(input);
    ExpName g = Expressions.makeExpName("g");
    ExpString a = Expressions.makeExpString("a");
    Exp gtable = Expressions.makeExpTableLookup(g, a);
    ExpString b = Expressions.makeExpString("b");
    Exp e = Expressions.makeExpTableLookup(gtable, b);
    Exp v = Expressions.makeExpName("x");
    Stat s = Expressions.makeStatAssign(Expressions.makeExpList(v),
                                        Expressions.makeExpList(e));
    Block expected = Expressions.makeBlock(s);
    assertEquals(expected, bl);
  }

  @Test
  void testmultistatements6() throws Exception {
    String input =
        "x = g.a.b ; ::mylabel:: do  y = 2 goto mylabel f=a(0,200) end break"; // same as testmultistatements0 except ;
    ASTNode c = parseAndShow(input);
    ExpName g = Expressions.makeExpName("g");
    ExpString a = Expressions.makeExpString("a");
    Exp gtable = Expressions.makeExpTableLookup(g, a);
    ExpString b = Expressions.makeExpString("b");
    Exp e = Expressions.makeExpTableLookup(gtable, b);
    Exp v = Expressions.makeExpName("x");
    Stat s0 = Expressions.makeStatAssign(v, e);
    StatLabel s1 = Expressions.makeStatLabel("mylabel");
    Exp y = Expressions.makeExpName("y");
    Exp two = Expressions.makeExpInt(2);
    Stat s2 = Expressions.makeStatAssign(y, two);
    Stat s3 = Expressions.makeStatGoto("mylabel");
    Exp f = Expressions.makeExpName("f");
    Exp ae = Expressions.makeExpName("a");
    Exp zero = Expressions.makeExpInt(0);
    Exp twohundred = Expressions.makeExpInt(200);
    List<Exp> args = Expressions.makeExpList(zero, twohundred);
    ExpFunctionCall fc = Expressions.makeExpFunCall(ae, args, null);
    StatAssign s4 = Expressions.makeStatAssign(f, fc);
    StatDo statdo = Expressions.makeStatDo(s2, s3, s4);
    StatBreak statBreak = Expressions.makeStatBreak();
    Block expectedBlock = Expressions.makeBlock(s0, s1, statdo, statBreak);
    Chunk expectedChunk = new Chunk(expectedBlock.firstToken, expectedBlock);
    assertEquals(expectedChunk, c);
  }
}
