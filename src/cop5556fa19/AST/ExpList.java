package cop5556fa19.AST;

import cop5556fa19.Token;
import java.util.List;

public class ExpList extends ASTNode {

  List<Exp> list;

  public ExpList(Token firstToken, List<Exp> list) {
    super(firstToken);
    this.list = list;
  }

  @Override
  public String toString() {
    return "ExpList [list=" + list + ", firstToken=" + firstToken + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((list == null) ? 0 : list.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ExpList other = (ExpList)obj;
    if (list == null) {
      return other.list == null;
    } else
      return list.equals(other.list);
  }

  @Override
  public Object visit(ASTVisitor v, Object arg) throws Exception {
    return v.visitExpList(this, arg);
  }
}
