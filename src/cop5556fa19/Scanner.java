
/* *
 * Developed  for the class project in COP5556 Programming Language Principles
 * at the University of Florida, Fall 2019.
 *
 * This software is solely for the educational benefit of students
 * enrolled in the course during the Fall 2019 semester.
 *
 * This software, and any software derived from it,  may not be shared with
 * others or posted to public web sites or repositories, either during the
 * course or afterwards.
 *
 *  @Beverly A. Sanders, 2019
 */
package cop5556fa19;

import static cop5556fa19.Token.Kind.*;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;

public class Scanner {

  private enum State {
    START,
    IN_NUMLIT,
    IN_IDENT,
    IN_COLON,
    IN_EQUAL,
    IN_COMMET,
    IN_DIV,
    IN_XOR,
    IN_GT,
    IN_LT,
    IN_DOT
  }

  Reader r;
  int currPos, currLine;
  int currChar;

  @SuppressWarnings("serial")
  public static class LexicalException extends Exception {
    public LexicalException(String arg0) { super(arg0); }
  }

  public Scanner(Reader r) throws IOException {
    this.r = new PushbackReader(r);
  }

  private void getNextChar() throws IOException {
    currChar = r.read();
    if (currChar != -1) {
      currPos++;
    }
  }

  private void unreadChar(int ch) throws IOException {
    if (ch != -1) {
      ((PushbackReader)r).unread(ch);
      currPos--;
    }
  }

  @SuppressWarnings("unused")
  private void debugCurr() {
    System.out.println(currPos + " " + currLine + " " + currChar);
  }

  private void skipWhitespace() throws IOException {
    do {
      getNextChar();
      if (currChar == '\r' || currChar == '\n') {
        if (currChar == '\r') {
          getNextChar();
          if (currChar != '\n') {
            unreadChar(currChar);
          }
        }
        currPos = 0;
        currLine++;
      }
    } while (Character.isWhitespace(currChar));
  }

  private Token getStringLit() throws Exception {
    StringBuffer sb = new StringBuffer();
    int endSym = currChar;
    sb.append((char)currChar);
    int startPos = currPos - 1;
    getNextChar();
    while (currChar != endSym) {
      if (currChar == -1 || currChar == '\0') { // :: throw a error
        throw new LexicalException("string not end: " + (char)endSym);
      } else if (0x5c == currChar) { // ascii for \
        getNextChar();
        if ('a' == currChar) {
          sb.append((char)0x7);
        } else if ('b' == currChar) {
          sb.append((char)0x8);
        } else if ('f' == currChar) {
          sb.append((char)0xc);
        } else if ('r' == currChar) {
          sb.append((char)0xd);
        } else if ('t' == currChar) {
          sb.append((char)0x9);
        } else if ('v' == currChar) {
          sb.append((char)0xb);
        } else if ('n' == currChar) {
          sb.append((char)0xa);
        } else if (0x5c == currChar) { // '\'
          sb.append((char)0x5c);
        } else if (0x22 == currChar) { // '"'
          if (currChar == endSym) {
            sb.append((char)0x5c);
          }
          sb.append((char)0x22);
        } else if (0x27 == currChar) { // "'"
          if (currChar == endSym) {
            sb.append((char)0x5c);
          }
          sb.append((char)0x27);
        } else {
          throw new LexicalException("unknown escape seqences: \\" +
                                     (char)currChar);
        }
      } else {
        sb.append((char)currChar);
      }
      getNextChar();
    }
    sb.append((char)currChar);
    return new Token(STRINGLIT, sb.toString(), startPos, currLine);
  }

  public Token getNext() throws Exception {
    // replace this code. Just for illustration
    // if (r.read() == -1) { return new Token(EOF,"eof",0,0);}
    // throw new LexicalException("Useful error message");
    Token t = null;
    State state = State.START;
    while (t == null) {
      switch (state) {
      case START: {
        skipWhitespace();
        // debugCurr();
        switch (currChar) {
        case -1: {
          t = new Token(EOF, "eof", currPos, currLine);
        } break;
        case '+': {
          t = new Token(OP_PLUS, "+", currPos - 1, currLine);
        } break;
        case '-': {
          state = State.IN_COMMET;
        } break;
        case '*': {
          t = new Token(OP_TIMES, "*", currPos - 1, currLine);
        } break;
        case '/': {
          state = State.IN_DIV;
        } break;
        case '%': {
          t = new Token(OP_MOD, "%", currPos - 1, currLine);
        } break;
        case '^': {
          t = new Token(OP_POW, "^", currPos - 1, currLine);
        } break;
        case '#': {
          t = new Token(OP_HASH, "#", currPos - 1, currLine);
        } break;
        case '&': {
          t = new Token(BIT_AMP, "&", currPos - 1, currLine);
        } break;
        case '~': {
          state = State.IN_XOR;
          // t = new Token(BIT_XOR, "~", currPos, currLine);
        } break;
        case '|': {
          t = new Token(BIT_OR, "|", currPos - 1, currLine);
        } break;
        case '>': {
          state = State.IN_GT;
        } break;
        case '<': {
          state = State.IN_LT;
        } break;
        case ',': {
          t = new Token(COMMA, ",", currPos - 1, currLine);
        } break;
        case ';': {
          t = new Token(SEMI, ";", currPos - 1, currLine);
        } break;
        case '.': {
          state = State.IN_DOT;
        } break;
        case ')': {
          t = new Token(RPAREN, ")", currPos - 1, currLine);
        } break;
        case '(': {
          t = new Token(LPAREN, "(", currPos - 1, currLine);
        } break;
        case '{': {
          t = new Token(LCURLY, "{", currPos - 1, currLine);
        } break;
        case '}': {
          t = new Token(RCURLY, "}", currPos - 1, currLine);
        } break;
        case '[': {
          t = new Token(LSQUARE, "[", currPos - 1, currLine);
        } break;
        case ']': {
          t = new Token(RSQUARE, "]", currPos - 1, currLine);
        } break;
        case ':': {
          state = State.IN_COLON;
        } break;
        case '=': {
          state = State.IN_EQUAL;
        } break;
        case '0': {
          t = new Token(INTLIT, "0", currPos - 1, currLine);
        } break;
        case '"': {
          // t = getStringLit('"');
        }
        // break;
        case '\'': {
          t = getStringLit();
        } break;
        default:
          if (Character.isDigit(currChar)) {
            state = State.IN_NUMLIT;
          } else if (Character.isJavaIdentifierStart(currChar)) {
            state = State.IN_IDENT;
          } else {
            throw new LexicalException("unknown char:" + (char)currChar +
                                       "in state " + state);
          }
        }
      } break;
      case IN_NUMLIT: {
        StringBuffer sb = new StringBuffer();
        int startPos = currPos - 1;
        sb.append((char)currChar);
        getNextChar();
        while (Character.isDigit(currChar)) {
          sb.append((char)currChar);
          getNextChar();
        }
        try {
          Integer.parseInt(sb.toString());
          t = new Token(INTLIT, sb.toString(), startPos, currLine);
        } catch (NumberFormatException e) {
          throw new LexicalException("number out of range");
        } finally {
          unreadChar(currChar);
          state = State.START;
        }
      } break;
      case IN_IDENT: {
        StringBuffer sb = new StringBuffer();
        int startPos = currPos - 1;
        sb.append((char)currChar);
        getNextChar();
        while (Character.isJavaIdentifierPart(currChar)) {
          sb.append((char)currChar);
          getNextChar();
        }
        unreadChar(currChar);
        // :: check if it is a keyword
        switch (sb.toString()) {
        case "and": {
          t = new Token(KW_and, "and", startPos, currLine); // this is ugly
        } break;
        case "break": {
          t = new Token(KW_break, "break", startPos, currLine);
        } break;
        case "do": {
          t = new Token(KW_do, "do", startPos, currLine);
        } break;
        case "else": {
          t = new Token(KW_else, "else", startPos, currLine);
        } break;
        case "elseif": {
          t = new Token(KW_elseif, "elseif", startPos, currLine);
        } break;
        case "end": {
          t = new Token(KW_end, "end", startPos, currLine);
        } break;
        case "false": {
          t = new Token(KW_false, "false", startPos, currLine);
        } break;
        case "for": {
          t = new Token(KW_for, "for", startPos, currLine);
        } break;
        case "function": {
          t = new Token(KW_function, "function", startPos, currLine);
        } break;
        case "goto": {
          t = new Token(KW_goto, "goto", startPos, currLine);
        } break;
        case "if": {
          t = new Token(KW_if, "if", startPos, currLine);
        } break;
        case "in": {
          t = new Token(KW_in, "in", startPos, currLine);
        } break;
        case "local": {
          t = new Token(KW_local, "local", startPos, currLine);
        } break;
        case "nil": {
          t = new Token(KW_nil, "nil", startPos, currLine);
        } break;
        case "not": {
          t = new Token(KW_not, "not", startPos, currLine);
        } break;
        case "or": {
          t = new Token(KW_or, "or", startPos, currLine);
        } break;
        case "repeat": {
          t = new Token(KW_repeat, "repeat", startPos, currLine);
        } break;
        case "return": {
          t = new Token(KW_return, "return", startPos, currLine);
        } break;
        case "then": {
          t = new Token(KW_then, "then", startPos, currLine);
        } break;
        case "true": {
          t = new Token(KW_true, "true", startPos, currLine);
        } break;
        case "until": {
          t = new Token(KW_until, "until", startPos, currLine);
        } break;
        case "while": {
          t = new Token(KW_while, "while", startPos, currLine);
        } break;
        default: {
          t = new Token(NAME, sb.toString(), startPos, currLine);
        }
        }
        state = State.START;
      } break;
      case IN_XOR: {
        int startPos = currPos - 1;
        getNextChar();
        if ('=' == currChar) {
          t = new Token(REL_NOTEQ, "~=", startPos, currLine);
        } else {
          unreadChar(currChar);
          t = new Token(BIT_XOR, "~", startPos, currLine);
        }
        state = State.START;
      } break;
      case IN_GT: {
        int startPos = currPos - 1;
        getNextChar();
        if ('=' == currChar) {
          t = new Token(REL_GE, ">=", startPos, currLine);
        } else if ('>' == currChar) {
          t = new Token(BIT_SHIFTR, ">>", startPos, currLine);
        } else {
          unreadChar(currChar);
          t = new Token(REL_GT, ">", startPos, currLine);
        }
        state = State.START;
      } break;
      case IN_LT: {
        int startPos = currPos - 1;
        getNextChar();
        if ('=' == currChar) {
          t = new Token(REL_LE, "<=", startPos, currLine);
        } else if ('<' == currChar) {
          t = new Token(BIT_SHIFTL, "<<", startPos, currLine);
        } else {
          unreadChar(currChar);
          t = new Token(REL_LT, "<", startPos, currLine);
        }
        state = State.START;
      } break;
      case IN_DOT: {
        int startPos = currPos - 1;
        getNextChar();
        if ('.' == currChar) {
          getNextChar();
          if ('.' == currChar) {
            t = new Token(DOTDOTDOT, "...", startPos, currLine);
          } else {
            unreadChar(currChar);
            t = new Token(DOTDOT, "..", startPos, currLine);
          }
        } else {
          unreadChar(currChar);
          t = new Token(DOT, ".", startPos, currLine);
        }
        state = State.START;
      } break;
      case IN_DIV: {
        int startPos = currPos - 1;
        getNextChar();
        if ('/' == currChar) {
          t = new Token(OP_DIVDIV, "//", startPos, currLine);
        } else {
          unreadChar(currChar);
          t = new Token(OP_DIV, "/", startPos, currLine);
        }
        state = State.START;
      } break;
      case IN_COMMET: {
        int startPos = currPos - 1;
        getNextChar();
        if ('-' == currChar) {
          // get comment
          StringBuffer sb = new StringBuffer("comment: ");
          skipWhitespace();
          while (!(currChar == '\r' || currChar == '\n')) {
            sb.append((char)currChar);
            getNextChar();
          }
          if (currChar == '\r') {
            getNextChar();
            if (currChar != '\n') {
              unreadChar(currChar);
            }
          }
          currLine++;
          currPos = 0;
          System.out.println(sb); // :: should for debug only
        } else {
          t = new Token(OP_MINUS, "-", startPos, currLine);
          unreadChar(currChar);
        }
        state = State.START; // comment is not token, reset state
      } break;
      case IN_COLON: {
        int startPos = currPos - 1;
        getNextChar();
        if (':' == currChar) {
          t = new Token(COLONCOLON, "::", startPos, currLine);
        } else {
          t = new Token(COLON, ":", startPos, currLine);
          unreadChar(currChar);
          // :: check this should not consume the currChar into void space
        }
        state = State.START;
      } break;
      case IN_EQUAL: {
        int startPos = currPos - 1;
        getNextChar();
        if ('=' == currChar) {
          t = new Token(REL_EQEQ, "==", startPos, currLine);
        } else {
          t = new Token(ASSIGN, "=", startPos, currLine);
          unreadChar(currChar);
          // :: same as double colon
        }
        state = State.START;
      } break;
      default:
        throw new LexicalException("unknown state:" + state);
      } // end switch
    }   // end while

    // System.out.println(t.toString()); // TODO for debug
    return t;
  }
}
