/**
 * Developed  for the class project in COP5556 Programming Language Principles
 * at the University of Florida, Fall 2019.
 *
 * This software is solely for the educational benefit of students
 * enrolled in the course during the Fall 2019 semester.
 *
 * This software, and any software derived from it,  may not be shared with
 * others or posted to public web sites, either during the course or afterwards.
 *
 *  @Beverly A. Sanders, 2019
 */

package cop5556fa19;

import cop5556fa19.AST.*;
import cop5556fa19.Token.Kind;

import java.util.*;

import static cop5556fa19.Token.Kind.*;

public class Parser {

  private static final Map<Kind, int[]> biOpMap;
  static {
    Map<Kind, int[]> aMap = new HashMap<Kind, int[]>();
    aMap.put(OP_PLUS, new int[] {10, 10});
    aMap.put(OP_MINUS, new int[] {10, 10});
    aMap.put(OP_TIMES, new int[] {11, 11});
    aMap.put(OP_MOD, new int[] {11, 11});
    aMap.put(OP_DIV, new int[] {11, 11});
    aMap.put(OP_DIVDIV, new int[] {11, 11});
    aMap.put(OP_POW, new int[] {14, 13});
    aMap.put(BIT_AMP, new int[] {6, 6});
    aMap.put(BIT_OR, new int[] {4, 4});
    aMap.put(BIT_XOR, new int[] {5, 5});
    aMap.put(BIT_SHIFTL, new int[] {7, 7});
    aMap.put(BIT_SHIFTR, new int[] {7, 7});
    aMap.put(DOTDOT, new int[] {9, 8});
    aMap.put(REL_EQEQ, new int[] {3, 3});
    aMap.put(REL_LE, new int[] {3, 3});
    aMap.put(REL_LT, new int[] {3, 3});
    aMap.put(REL_GT, new int[] {3, 3});
    aMap.put(REL_GE, new int[] {3, 3});
    aMap.put(REL_NOTEQ, new int[] {3, 3});
    aMap.put(KW_and, new int[] {2, 2});
    aMap.put(KW_or, new int[] {1, 1});

    biOpMap = Collections.unmodifiableMap(aMap);
  }

  @SuppressWarnings("serial")
  class SyntaxException extends Exception {
    Token t;

    public SyntaxException(Token t, String message) {
      super(t.line + ":" + t.pos + " " + message);
    }
  }

  final Scanner scanner;
  Token t; // invariant:  this is the next token

  Parser(Scanner s) throws Exception {
    this.scanner = s;
    t = scanner.getNext(); // establish invariant
  }
  // copy from lua parser https://github.com/lua/lua/blob/master/lparser.c
  Exp exp() throws Exception { return subexpr(0); }

  Chunk parse() throws Exception { return chunk(); }
  /**
   * subexpr -> (simpleexp | unop subexpr){binop subexpr}
   * @return
   * @throws Exception
   */
  private Exp subexpr(int limit) throws Exception {
    Token first = t;
    Exp e0 = null;
    switch (first.kind) {
      // unop -> - | not | # | ~
    case OP_MINUS:
    case KW_not:
    case OP_HASH:
    case BIT_XOR: {
      consume();
      e0 = subexpr(limit);
      e0 = new ExpUnary(first, first.kind, e0);
    } break;
    default:
      e0 = simpleexp();
    }
    // binop -> ...
    while (
        isKind(OP_PLUS, OP_MINUS, OP_TIMES, OP_DIV, OP_DIVDIV, OP_POW, OP_MOD,
               BIT_AMP, BIT_OR, BIT_XOR, BIT_SHIFTL, BIT_SHIFTR, DOTDOT, REL_GE,
               REL_GT, REL_LE, REL_LT, REL_EQEQ, REL_NOTEQ, KW_and, KW_or) &&
        biOpMap.get(t.kind)[0] > limit) { // && biOpMap.get(op.kind)[0] > limit
      Token op = t;
      consume();
      Exp e1 = subexpr(biOpMap.get(op.kind)[1]);
      e0 = new ExpBinary(first, e0, op.kind, e1);
    }
    return e0;
  }

  /**
   * simpleexp -> nil | false | true | INT | STRING
   *              | ... | functiondef | prefixexp | tableconstructor
   * @return
   * @throws Exception
   */
  private Exp simpleexp() throws Exception {

    Token first = t;
    Exp e = null;
    switch (first.kind) {
    case KW_nil: {
      e = new ExpNil(first);
      consume();
    } break;
    case KW_false: {
      e = new ExpFalse(first);
      consume();
    } break;
    case KW_true: {
      e = new ExpTrue(first);
      consume();
    } break;
    case INTLIT: {
      e = new ExpInt(first);
      consume();
    } break;
    case STRINGLIT: {
      e = new ExpString(first);
      consume();
    } break;
    case DOTDOTDOT: {
      e = new ExpVarArgs(first);
      consume();
    } break;
    case KW_function: {
      e = functionExp();
    } break;
    case LCURLY: {
      e = tableExp();
    } break;
    default:
      e = prefixTailExp();
    }
    return e;
  }

  /**
   * tableconstructor -> { [list of field] }
   * @return
   * @throws Exception
   */
  private Exp tableExp() throws Exception {
    Token first = t;
    List<Field> lf = new ArrayList<Field>();
    match(LCURLY);
    while (!isKind(RCURLY)) {
      Field f = fieldExp();
      lf.add(f);
      while (isKind(COMMA, SEMI)) {
        consume();
        if (isKind(RCURLY))
          break;
        f = fieldExp();
        lf.add(f);
      }
    }
    match(RCURLY);
    return new ExpTable(first, lf);
  }

  /**
   * field ::= ‘[’ exp ‘]’ ‘=’ exp | Name ‘=’ exp | exp
   * @return
   * @throws Exception
   */
  private Field fieldExp() throws Exception {
    Token first = t;
    Field f = null;
    if (isKind(LSQUARE)) {
      match(LSQUARE);
      Exp k = subexpr(0);
      match(RSQUARE);
      match(ASSIGN);
      Exp v = subexpr(0);
      f = new FieldExpKey(first, k, v);
    } else if (isKind(NAME)) {
      Token tn = match(NAME);
      if (isKind(ASSIGN)) {
        consume();
        Name n = new Name(tn, tn.text);
        Exp v = subexpr(0);
        f = new FieldNameKey(first, n, v);
      } else {
        ExpName ne = new ExpName(tn.text);
        f = new FieldImplicitKey(first, ne);
      }
    } else {
      Exp v = subexpr(0);
      f = new FieldImplicitKey(first, v);
    }
    return f;
  }

  /**
   * functiondef ::= function funcbody
   *
   * @return
   * @throws Exception
   */
  private Exp functionExp() throws Exception {
    Token first = t;
    consume(); // skip 'function'
    FuncBody fb = funcBody();
    return new ExpFunction(first, fb);
  }

  /**
   * parlist ::= namelist [‘,’ ‘...’] | ‘...’
   * namelist ::= Name {‘,’ Name}
   * @return
   * @throws Exception
   */
  private ParList parlistExp() throws Exception {
    Token first = t;
    // List<Name> pl = new ArrayList<Name>();
    List<Name> nl = new ArrayList<Name>();
    boolean hasVarArgs = false;
    if (isKind(DOTDOTDOT)) {
      hasVarArgs = true;
      consume();
    } else if (isKind(NAME)) {
      // nl = nameList();
      nl = new ArrayList<Name>();
      nl.add(new Name(t, t.text));
      consume();
      while (isKind(COMMA)) {
        consume();
        if (isKind(DOTDOTDOT)) {
          hasVarArgs = true;
          consume();
          break;
        }
        Token n = match(NAME);
        nl.add(new Name(n, n.text));
      }
    }
    ParList pl = new ParList(first, nl, hasVarArgs);
    return pl;
  }

  /**
   * prefixexp ::= Name | ‘(’ exp ‘)’
   * @return
   * @throws Exception
   */
  private Exp prefixExp() throws Exception {
    Token first = t;
    Exp e = null;
    if (isKind(NAME)) {
      e = new ExpName(first.text);
      consume();
    } else if (isKind(LPAREN)) {
      consume();
      e = exp();
      match(RPAREN);
    } else {
      error(first, "expected a NAME or (:");
    }
    return e;
  }

  private Exp prefixTailExp() throws Exception {
    Token first = t;
    Exp e = prefixExp();
    for (;;) {
      Token nowToken = t;
      switch (nowToken.kind) {
      case DOT: { // var -> prefixexp . name; tablelookup
        consume();
        ExpString k = new ExpString(match(NAME));
        e = new ExpTableLookup(first, e, k);
      } break;
      case COLON: {
        consume();
        Token name = match(NAME);
        Exp tmp = new ExpTableLookup(first, e, new ExpName(name));
        // call funcargs();
        List<Exp> args = funcArgs();
        args.add(0, e); // add current expr(table) to the front
        e = new ExpFunctionCall(first, tmp, args);
        // then update e with function call
      } break;
      case LSQUARE: { // "[" -> tablelookup
        consume();
        Exp k = exp();
        match(RSQUARE);
        e = new ExpTableLookup(first, e, k);
      } break;
      case LPAREN:
      case LCURLY:
      case STRINGLIT: {
        List<Exp> args = funcArgs();
        e = new ExpFunctionCall(first, e, args);
      } break;
      default:
        return e;
      }
    }
  }

  private List<Exp> expList() throws Exception {
    List<Exp> exps = new ArrayList<Exp>();
    exps.add(exp());
    while (isKind(COMMA)) {
      consume();
      exps.add(exp());
    }
    return exps;
  }

  private List<Exp> funcArgs() throws Exception {
    List<Exp> args = new ArrayList<Exp>();
    Token first = t;
    switch (t.kind) {
    case LPAREN: {
      consume();
      if (!isKind(RPAREN)) {
        args = expList();
      }
      match(RPAREN);
    } break;
    case LCURLY: {
      Exp e = tableExp();
      args.add(e);
    } break;
    case STRINGLIT: {
      Exp e = new ExpString(first);
      consume();
      args.add(e);
    } break;
    default:
      error(first, "function args expected");
    }
    return args;
  }

  // the block had to know where to stop
  Block block_with_end(Kind... endKind) throws Exception {
    Token first = t;
    List<Stat> stats = new ArrayList<Stat>();
    while (!isKind(endKind)) {
      Stat tmp = statement();
      if (tmp != null) {
        stats.add(tmp);
        if (tmp.firstToken.kind == KW_return)
          break;
      }
    }
    return new Block(first, stats);
  }

  Block block() throws Exception { return block_with_end(EOF); }

  Chunk chunk() throws Exception {
    Token first = t;
    Block b = block();
    return new Chunk(first, b);
  }

  /**
   * statements
   */

  private Stat statement() throws Exception {
    Token first = t;
    Stat stat = null;
    switch (t.kind) {
    case SEMI: {
      consume();
    } break;
    case COLONCOLON: { // stat -> label
      consume();
      // Token tName = match(NAME);
      Name name = new Name(match(NAME));
      match(COLONCOLON);
      stat = new StatLabel(first, name);
    } break;
    case KW_break: { // stat -> break
      stat = new StatBreak(first);
      consume();
    } break;
    case KW_goto: { // stat -> goto Name
      consume();
      Name name = new Name(match(NAME));
      stat = new StatGoto(first, name);
    } break;
    case KW_do: { // stat -> do block end
      consume();
      Block b = block_with_end(KW_end); // TODO: check after
      match(KW_end);
      stat = new StatDo(first, b);
    } break;
    case KW_while: {
      stat = whileStatement();
    } break;
    case KW_repeat: {
      stat = repeatStatement();
    } break;
    case KW_if: {
      stat = ifStatement();
    } break;
    case KW_for: {
      stat = forStatement();
    } break;
    case KW_function: {
      stat = funcStatement();
    } break;
    case KW_local: {
      consume();
      if (isKind(KW_function)) {
        // stat = localFuncStatement();
        Token lf = consume();
        FuncName fnName = funcName();
        FuncBody fnBody = funcBody();
        return new StatLocalFunc(lf, fnName, fnBody);
      } else {
        // stat = localStatement();
        // assignStatement;
        List<ExpName> names = nameList();
        List<Exp> exps = null;

        if (isKind(ASSIGN)) {
          consume();
          exps = expList();
        }
        stat = new StatLocalAssign(first, names, exps);
      }
    } break;
    case KW_return: {
      consume(); // skip the return
      List<Exp> exps = new ArrayList<Exp>();
      if (isKind(SEMI)) { // empty return
        consume();
      } else {
        exps = expList(); // optional expList;
      }

      if (isKind(SEMI))
        consume();
      stat = new RetStat(first, exps);
    } break;
    default:
      List<Exp> vars = varList();
      match(ASSIGN);
      List<Exp> exps = expList();
      stat = new StatAssign(first, vars, exps);
    }
    return stat;
  }

  private List<ExpName> nameList() throws Exception {
    List<ExpName> names = new ArrayList<ExpName>();
    names.add(new ExpName(match(NAME)));
    while (isKind(COMMA)) {
      consume();
      names.add(new ExpName(match(NAME)));
    }
    return names;
  }

  private List<Exp> varList() throws Exception {
    List<Exp> vars = new ArrayList<Exp>();
    vars.add(prefixTailExp());
    while (isKind(COMMA)) {
      consume();
      vars.add(prefixTailExp());
    }
    return vars;
  }

  private FuncName funcName() throws Exception {
    Token first = t;
    List<ExpName> names = new ArrayList<ExpName>();
    ExpName aftColon = null;
    names.add(new ExpName(match(NAME)));
    while (isKind(DOT)) {
      consume(); // bug fix
      names.add(new ExpName(match(NAME)));
    }
    if (isKind(COLON)) {
      consume(); // bug fix
      aftColon = new ExpName(match(NAME));
    }
    return new FuncName(first, names, aftColon);
  }

  private FuncBody funcBody() throws Exception {
    Token first = t;
    match(LPAREN);
    ParList pe = parlistExp();
    match(RPAREN);
    Block b = block_with_end(KW_end);
    match(KW_end);
    return new FuncBody(first, pe, b);
  }

  private Stat funcStatement() throws Exception {
    Token first = t;
    match(KW_function);
    FuncName fnName = funcName();
    FuncBody fnBody = funcBody();
    return new StatFunction(first, fnName, fnBody);
  }

  private Stat forStatement() throws Exception {
    Token first = t;
    match(KW_for);
    Token name = match(NAME);
    if (isKind(ASSIGN)) { // StatFor
      Exp ebeq = null;
      Exp eend = null;
      Exp einc = null;
      Block b = null;
      consume();
      ebeq = exp();
      match(COMMA);
      eend = exp();
      if (isKind(COMMA)) {
        consume(); // bug fix
        einc = exp();
      }
      match(KW_do);
      b = block_with_end(KW_end);
      match(KW_end);
      return new StatFor(first, new ExpName(name), ebeq, eend, einc, b);
    } else { // it must be StatForEach
      List<ExpName> names = new ArrayList<ExpName>();
      List<Exp> exps = new ArrayList<Exp>();
      Block b = null;
      names.add(new ExpName(name));
      while (isKind(COMMA)) {
        consume(); // skip the comma
        names.add(new ExpName(match(NAME)));
      }
      match(KW_in);
      exps = expList();
      match(KW_do);
      b = block_with_end(KW_end);
      match(KW_end);
      return new StatForEach(first, names, exps, b);
    }
  }

  private Stat ifStatement() throws Exception {
    Token first = t;
    match(KW_if);
    List<Exp> es = new ArrayList<Exp>();
    List<Block> bs = new ArrayList<Block>();
    Exp e1 = exp();
    match(KW_then);
    Block b1 = block_with_end(KW_else, KW_elseif);
    es.add(e1);
    bs.add(b1);
    while (isKind(KW_elseif)) {
      consume();
      Exp tmpE = exp();
      match(KW_then);
      Block tmpB = block_with_end(KW_else, KW_elseif, KW_end);
      es.add(tmpE);
      bs.add(tmpB);
    }
    if (isKind(KW_else)) {
      consume(); // bug fix
      Block belse = block_with_end(KW_end);
      bs.add(belse);
    }
    match(KW_end); // consume the end
    return new StatIf(first, es, bs);
  }

  private Stat repeatStatement() throws Exception {
    Token first = t;
    match(KW_repeat);
    Block b = block_with_end(KW_until);
    match(KW_until);
    Exp e = exp();

    return new StatRepeat(first, b, e);
  }

  private Stat whileStatement() throws Exception {
    Token first = t;
    match(KW_while);
    Exp e = exp();
    match(KW_do);
    Block b = block_with_end(KW_end);
    match(KW_end);

    return new StatWhile(first, e, b);
  }

  protected boolean isKind(Kind kind) { return t.kind == kind; }

  protected boolean isKind(Kind... kinds) {
    for (Kind k : kinds) {
      if (k == t.kind)
        return true;
    }
    return false;
  }

  /**
   * @param kind
   * @return
   * @throws Exception
   */
  Token match(Kind kind) throws Exception {
    Token tmp = t;
    if (isKind(kind)) {
      consume();
      return tmp;
    }
    error(kind);
    return null; // unreachable
  }

  /**
   * @param
   * @return
   * @throws Exception
   */
  Token match(Kind... kinds) throws Exception {
    Token tmp = t;
    if (isKind(kinds)) {
      consume();
      return tmp;
    }
    StringBuilder sb = new StringBuilder();
    for (Kind kind1 : kinds) {
      sb.append(kind1).append(kind1).append(" ");
    }
    error(kinds);
    return null; // unreachable
  }

  Token consume() throws Exception {
    Token tmp = t;
    t = scanner.getNext();
    return tmp;
  }

  void error(Kind... expectedKinds) throws SyntaxException {
    String kinds = Arrays.toString(expectedKinds);
    String message;
    if (expectedKinds.length == 1) {
      message = "Expected " + kinds + " at " + t.line + ":" + t.pos;
    } else {
      message = "Expected one of" + kinds + " at " + t.line + ":" + t.pos;
    }
    throw new SyntaxException(t, message);
  }

  void error(Token t, String m) throws SyntaxException {
    String message = m + " at " + t.line + ":" + t.pos;
    throw new SyntaxException(t, message);
  }
}
